<?php

function shift($array_q)
{
    for ($i = 1; $i < count($array_q); $i++) $array_q[$i - 1] = $array_q[$i];
    array_pop($array_q);
    return $array_q;
}

function lfsr_gf_2($array_q, $curr_points, $file)
{
    $addition_points = 0;
    foreach ($curr_points as $item) $addition_points += $array_q[$item];
    $addition_points %= 2;
    return processingData($array_q, $addition_points, $file);
}

function lfsr_gf_3($array_q, $curr_points, $file)
{
    $addition_points = 0;
    foreach ($curr_points as $item) {
        $item ? $addition_points += $array_q[$item] : $addition_points += (2 * $array_q[$item]);
    }
    $addition_points %= 3;
    return processingData($array_q, $addition_points, $file);
}

function processingData($array_q, $addition_points, $file)
{
    array_push($array_q, $addition_points);
    $array_q = shift($array_q);
    fwrite($file, implode(',', $array_q) . "\n");
    return $array_q;
}

function switch_lfsr($array_q, $curr_points, $file, $mod)
{
    if ($mod === 3) return lfsr_gf_3($array_q, $curr_points, $file);
    else if ($mod === 2) return lfsr_gf_2($array_q, $curr_points, $file);
}

function shift_register($array_q, $curr_points, $check, $file, $mod)
{
    $array_q = switch_lfsr($array_q, $curr_points, $file, $mod);
    $period = 1;
    while (array_diff($array_q, $check)) {
        $array_q = switch_lfsr($array_q, $curr_points, $file, $mod);
        $period++;
    }
    print($period);
    fwrite($file, $period . "\n");
}

function create_array_with_deg_of_pol($size)
{
    $array = [];
    for ($i = 0; $i < $size; $i++) $array[] = 1;
    return $array;
}

function start($deg_pol, $removal_points, $gf_mod, $file_name)
{
    $file = fopen($file_name, 'w');
    $array_q = create_array_with_deg_of_pol($deg_pol);
    $check = create_array_with_deg_of_pol($deg_pol);
    shift_register($array_q, $removal_points, $check, $file, $gf_mod);
    fclose($file);
}

$gf_mod = 2;
$deg_pol = 20;
$removal_points = [3, 0];
$file_name = 'result_gf_2.txt';
start($deg_pol, $removal_points, $gf_mod, $file_name);

$gf_mod = 3;
$deg_pol = 5;
$removal_points = [1, 0];
$file_name = 'result_gf_3.txt';

start($deg_pol, $removal_points, $gf_mod, $file_name);
