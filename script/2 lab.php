<?php

function shift($array_q)
{
    for ($i = 1; $i < count($array_q); $i++) $array_q[$i - 1] = $array_q[$i];
    array_pop($array_q);
    return $array_q;
}

function nonlinear_lfsr($array_q, $file)
{
    $count_1 = 0;
    $count_0 = 0;

    for ($i = 0; $i < 1000; $i++) {
        $add_point =(int) ($array_q[16] xor (($array_q[15] xor $array_q[11]) + ($array_q[9] + $array_q[5]) * $array_q[10] * ($array_q[9] + $array_q[5]) * $array_q[0]));
        array_push($array_q, $add_point);
        $array_q = shift($array_q);
        $array_q[0] ? $count_1++ : $count_0++;
        fwrite($file, implode($array_q, ',') . "\n");
    }
    print($count_0 . "\n");
    print($count_1 . "\n");
    fwrite($file, $count_0 . "\n");
    fwrite($file, $count_1 . "\n");
}

function start($array_q)
{
    $file_name = 'nonlinear_lfsr.txt';
    $file = fopen($file_name, 'w');
    nonlinear_lfsr($array_q, $file);
    fclose($file);
}

//$array_q = [0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1];
$array_q = [1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1];

start($array_q);



