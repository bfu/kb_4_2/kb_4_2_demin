n = 5  # n - степень порождающего многочлена

currentPoints = [1, 0]  # точки съема и файл вывода для задания 2
f = open('nlsfr.txt', 'w')


#
# currentPoints = [12, 10, 1, 0]  # точки съема и файл вывода для задания 1
# f = open('result.txt', 'w')


def shift(q):  # сдвиг массива на 1 элемент
    for i in range(1, len(q)):
        q[i - 1] = q[i]
    q.pop()
    return (q)


def nlfsr(q, f):
    ones = 0
    nulls = 0
    # for i in range(len(q)):
    #     q[i] = bool(q[i])
    for i in range(1000):
        additionPoints = q[16] != ((q[15] != q[11]) + ((q[9] + q[5]) * q[10] * (q[9] + q[5]) * q[0]))
        q.append(additionPoints)
        q = shift(q)
        if q[0]:
            ones += 1
        else:
            nulls += 1
        f.write(str(q) + "\n")
    f.write(str(ones) + " " + str(nulls))


def lfsr(q, curr, f):  # добавляем элемент в массив для (для задачи GF(2))
    flag = 0
    additionPoints = 0
    for i in curr:
        additionPoints += q[i]
    additionPoints = additionPoints % 2
    q.append(additionPoints)
    q = shift(q)
    f.write(str(q) + "\n")
    return q


def lfsr3(q, curr, f):  # добавляем элемент в массив для (для задачи GF(3))
    flag = 0
    additionPoints = 0
    for i in curr:
        if i != 0:
            additionPoints += q[i]
        else:
            additionPoints += 2 * q[i]
    additionPoints = additionPoints % 3
    q.append(additionPoints)
    q = shift(q)
    f.write(str(q) + "\n")
    return q


def roundabout(q, currentPoints, f, check):  # регистр сдвига
    period = 1
    while q != check:
        # lfsr(q, currentPoints, f)  # по модулю 2
        lfsr3(q, currentPoints, f)  # по модулю 3
        period += 1
    f.write(str(period))


def fill(size):  # создаем динамический массив размером = степени многочлена
    mas = []
    for i in range(size):
        mas.append(1)
    return mas


# q = [True,False,True,False,True,False,True,False,True,False,True,False,True,False,True,False,True]
q = [True,False,True,True,True,True,True,True,True,True,True,True,True,False,True,True,True]
nlfsr(q, f)
# q = fill(n)
# check = fill(n)
#
# q = lfsr3(q,currentPoints,f)
# # q = lfsr(q, currentPoints, f)
# roundabout(q, currentPoints, f, check)
# f.close()
