<?php

const filename = 'lfsr_reverse.txt';

class Foo
{
    public $array = [];

    public $s_0;

    public $points;

    public $file;

    function __construct($array, $s_0, $points, $file)
    {
        $this->array = $array;

        $this->s_0 = $s_0;

        $this->points = $points;

        $this->file = $file;

    }

    public function processing()
    {
        $sum = 0;

        foreach ($this->points as $item) $sum += $this->array[$item];

        $this->s_0 += $sum;

        array_unshift($this->array, $this->s_0 % 2);

        array_pop($this->array);

        $this->s_0 = intdiv($this->s_0, 2);

        fwrite($this->file, $this->get() . "\n");
    }

    public function get()
    {
        return implode($this->array) . ' ' . $this->s_0;
    }

}

function generateInitData()
{
    return [
        rand(0, 1),
        rand(0, 1),
        rand(0, 1),
        rand(0, 1),
        rand(0, 1),
        rand(0, 1),
    ];
}

function start($rand_s, $points, $file, $i)
{
    $random_array = generateInitData();

    $foo = new Foo($random_array, $rand_s[$i], $points, $file);

    fwrite($file, 'Начало: ' . "\n" . $foo->get() . "\n");

    $check_array = [];


    while (true) {
        array_push($check_array, $foo->get());

        $foo->processing();

        for ($q = 0; $q < count($check_array); $q++) {

            if ($foo->get() == $check_array[$q]) {
                fwrite($file, 'Период ' . (count($check_array) - $q) . "\n" .
                    'Нач. шаг ' . $q . "\n");
                return;
            }

        }
    }
}

$points = [1, 3, 5];

$rand_s = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];

$file = fopen(filename, 'w');

for ($i = 0; $i < 10; $i++) start($rand_s, $points, $file, $i);

fclose($file);
